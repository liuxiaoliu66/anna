package test

import mandysax.anna2.annotation.Value
import mandysax.anna2.factory.DefaultConverterFactory
import org.junit.Test

class TestModel {
    @Value("name")
    lateinit var name: String
}

class TestJson {
    @Test
    fun testJson() {
        val e = Exception("test不通过!")
        val json = "{\n" +
                "    \"name\":\"成长 (Older)\",\n" +
                "    \"id\":1889528361,\n" +
                "    \"pst\":0,\n" +
                "    \"t\":0,\n" +
                "    \"ar\":[\n" +
                "        {\n" +
                "            \"id\":1028104,\n" +
                "            \"name\":\"Alec Benjamin\",\n" +
                "            \"alias\":[\n" +
                "\n" +
                "            ]\n" +
                "        }\n" +
                "    ]\n" +
                "}"
        DefaultConverterFactory.create(TestModel::class.java, json).apply {
            if (name != "成长 (Older)") {
                throw e
            }
            println("test通过！")
        }
    }
}