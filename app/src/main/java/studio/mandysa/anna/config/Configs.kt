package studio.mandysa.anna

import mandysax.anna2.Anna2
import mandysax.anna2.factory.DataClassConverterFactory
import studio.mandysa.anna.api.NeteaseCloudMusicApi

const val BASE_URL = "http://cloud-music.pl-fe.cn/"

val api = Anna2.build().baseUrl(BASE_URL).addConverterFactory(DataClassConverterFactory())
    .create(NeteaseCloudMusicApi::class.java)