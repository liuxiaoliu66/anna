package studio.mandysa.anna

import android.annotation.SuppressLint
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.Column
import androidx.compose.material.Button
import androidx.compose.material.Text
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.lifecycle.lifecycleScope
import kotlinx.coroutines.launch

/**
 * @author 黄浩
 */
class MainActivity : ComponentActivity() {

    @SuppressLint("CoroutineCreationDuringComposition")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            Column {
                var cache by rememberSaveable() {
                    mutableStateOf("not")
                }
                var network by rememberSaveable() {
                    mutableStateOf("not")
                }
                Text(text = "缓存：$cache")
                Button(onClick = {
                    lifecycleScope.launch {
                        val beans = api.cache().getBannerList()
                        cache = beans.toString()
                    }
                }) {
                    Text("读取缓存")
                }
                Text(text = "网络：$network")
                Button(onClick = {
                    lifecycleScope.launch {
                        val beans = api.network().getBannerList()
                        network = beans.toString()
                    }
                }) {
                    Text("读取网络")
                }
            }
        }
    }
}