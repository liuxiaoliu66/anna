package studio.mandysa.anna.api

import com.drake.net.cache.CacheMode
import mandysax.anna2.annotation.CacheValidTime
import mandysax.anna2.annotation.EnableCache
import mandysax.anna2.annotation.Get
import mandysax.anna2.annotation.Path
import studio.mandysa.anna.bean.DataClassTestBean
import java.util.concurrent.TimeUnit

/**
 * @author 黄浩
 */
interface NeteaseCloudMusicApi {
    //主页轮播图
    @Get("banner?type=1")
    @Path("banners")
    suspend fun getBannerList(): List<DataClassTestBean>
    //修改DataClassTestBean为DefaultTestBean可以测试旧版本的解析器

    @EnableCache(CacheMode.READ)
    suspend fun cache(): NeteaseCloudMusicApi

    @EnableCache(CacheMode.REQUEST_THEN_READ)
    @CacheValidTime(1, TimeUnit.DAYS)
    suspend fun network(): NeteaseCloudMusicApi
}