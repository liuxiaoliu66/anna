package studio.mandysa.anna.bean

import mandysax.anna2.annotation.Path
import mandysax.anna2.annotation.Value

/**
 * Create by HuangHao at 2022/11/1
 */
class DefaultTestBean {
    @Value("pic")
    private lateinit var pic: String

    @Path("song/al")
    @Value("id")
    private lateinit var albumId: String

    @Value("encodeId")
    private lateinit var encodeId: String

    @Value("targetType")
    private val targetType = 0

    @Value("typeTitle")
    private lateinit var typeTitle: String

    @Value("url")
    private lateinit var url: String

    override fun toString(): String {
        return "DefaultTestBean(pic='$pic', albumId='$albumId', encodeId='$encodeId', targetType=$targetType, typeTitle='$typeTitle', url='$url')"
    }

}