package studio.mandysa.anna.bean

/**
 * @author 黄浩
 */
data class DataClassTestBean(
    val pic: String,
    val encodeId: String,
    val targetType: Int,
    val typeTitle: String,
    val url: String,
)