package studio.mandysa.anna

import android.app.Application
import com.drake.net.NetConfig
import okhttp3.Cache

class App : Application() {
    override fun onCreate() {
        super.onCreate()
        NetConfig.initialize(BASE_URL, this) {
            // ...
            // 本框架支持Http缓存协议和强制缓存模式
            cache(Cache(cacheDir, 1024 * 1024 * 128)) // 缓存设置, 当超过maxSize最大值会根据最近最少使用算法清除缓存来限制缓存大小
        }
    }
}