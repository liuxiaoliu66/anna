package mandysax.anna2.factory

/**
 * @author 黄浩
 *
 * 查找要解析为 json 的对象的工厂
 */
object ConverterFactory {

    private val mFactoryList = arrayListOf<Factory>()

    internal fun add(factory: Factory) {
        mFactoryList.add(factory)
    }

    internal fun <T : Any> create(clazz: Class<T>, json: String): T {
        try {
            for (f in mFactoryList) {
                if (f.parsingType(clazz))
                    return f.create(clazz, json)
            }
            return DefaultConverterFactory.create(clazz, json)
        } catch (e: IllegalStateException) {
            throw IllegalStateException("ConverterFactory cannot create data object for ${clazz.javaClass}")
        }
    }

    interface Factory {
        /**
         * 用于构建数据类
         *
         * @param clazz data class
         * @param json json
         * @return 构造好的data class实体
         */
        fun <T : Any> create(clazz: Class<T>, json: String): T

        /**
         * 判断这个工厂是否可以解析这个类
         *
         * @param clazz 要解析的class
         * @return 是否是工厂可以解析的数据类型
         */
        fun parsingType(clazz: Class<*>): Boolean
    }
}