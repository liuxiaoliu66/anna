package mandysax.anna2.factory

import mandysax.anna2.annotation.Path
import mandysax.anna2.annotation.Value
import mandysax.anna2.utils.Utils.parsing
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import org.json.JSONTokener
import java.lang.reflect.Field
import java.lang.reflect.ParameterizedType
import java.lang.reflect.Type


/**
 * @author 黄浩
 */
object DefaultConverterFactory : ConverterFactory.Factory {

    override fun parsingType(clazz: Class<*>): Boolean = true

    @Suppress("UNCHECKED_CAST")
    override fun <T : Any> create(clazz: Class<T>, json: String): T {
        when (clazz) {
            String::class.java -> {
                return json as T
            }

            Boolean::class.javaPrimitiveType -> {
                return json.toBoolean() as T
            }

            Int::class.javaPrimitiveType -> {
                return json.toInt() as T
            }

            Float::class.javaPrimitiveType -> {
                return json.toFloat() as T
            }

            Long::class.javaPrimitiveType -> {
                return json.toLong() as T
            }
        }
        val model: T = try {
            clazz.getDeclaredConstructor().newInstance()
        } catch (e: Exception) {
            throw e
        }
        for (field in model.javaClass.declaredFields) {
            field.isAccessible = true
            val name = field.getAnnotation(Value::class.java)?.value ?: continue
            val useJson = field.getAnnotation(Path::class.java)?.value?.split("/")?.let {
                parsing(json, it)
            } ?: json
            val value = when (field.type) {
                String::class.java -> {
                    parsingString(name, useJson)
                }

                Boolean::class.javaPrimitiveType -> {
                    parsingBoolean(name, useJson)
                }

                Int::class.javaPrimitiveType -> {
                    parsingInt(name, useJson)
                }

                Float::class.javaPrimitiveType -> {
                    parsingFloat(name, useJson)
                }

                Long::class.javaPrimitiveType -> {
                    parsingLong(name, useJson)
                }

                List::class.java -> {
                    parsingList(field, name, useJson)
                }

                Array::class.java -> {
                    val jsonArray = parsingJsonArray(name, useJson)
                    val array =
                        java.lang.reflect.Array.newInstance(field.type, jsonArray?.length() ?: 0)
                    for (i1 in 0 until (jsonArray?.length() ?: 0)) {
                        java.lang.reflect.Array.set(
                            array,
                            i1,
                            create(field.type, jsonArray!!.getString(i1))
                        )
                    }
                    array
                }

                else -> {
                    parsingObject(field, parsing(useJson, listOf(name)))
                }
            }
            field.set(model, value)
        }
        return model
    }

    private fun getNextValue(key: String): JSONObject {
        return try {
            when (val nextValue = JSONTokener(key).nextValue()) {
                is JSONObject -> nextValue
                else -> JSONArray(key).optJSONObject(0)
            }
        } catch (_: JSONException) {
            JSONObject()
        }
    }

    private fun parsingBoolean(name: String, json: String): Boolean {
        return getNextValue(json).optBoolean(name)
    }

    private fun parsingString(name: String, json: String): String {
        return getNextValue(json).optString(name)
    }

    private fun parsingInt(name: String, json: String): Int {
        return getNextValue(json).optInt(name)
    }

    private fun parsingLong(name: String, json: String): Long {
        return getNextValue(json).optLong(name)
    }

    private fun parsingJsonArray(name: String, json: String): JSONArray? {
        return getNextValue(json).optJSONArray(name)
    }

    private fun parsingObject(field: Field, json: String): Any? {
        return create(field.type, json)
    }

    private fun parsingFloat(name: String, json: String) = parsingString(name, json).toFloat()

    private fun parsingList(field: Field, name: String, json: String) = arrayListOf<Any>().apply {
        val array = parsingJsonArray(name, json)
        if (array != null)
            for (i in 0 until array.length()) {
                try {
                    this.add(create(field.getListGenericTypeClass(), array.getString(i)))
                } catch (ignored: JSONException) {
                }
            }
    }

    /**
     * 获取[List]的泛型类型
     */
    private fun Field.getListGenericTypeClass(): Class<*> {
        val genericsFieldType: Type = this.genericType
        if (genericsFieldType is ParameterizedType) {
            val fieldArgTypes: Array<Type> = genericsFieldType.actualTypeArguments
            for (fieldArgType in fieldArgTypes) {
                return fieldArgType as Class<*>
            }
        }
        throw RuntimeException("Cannot find the generic type of this " + this.name + " field")
    }

}