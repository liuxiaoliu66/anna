package mandysax.anna2.factory

import com.google.gson.Gson

/**
 * Create by HuangHao at 2022/11/1
 */
class DataClassConverterFactory(private val gson: Gson = Gson()) : ConverterFactory.Factory {
    override fun <T : Any> create(clazz: Class<T>, json: String): T {
        return gson.fromJson(json, clazz)
    }

    override fun parsingType(clazz: Class<*>): Boolean {
        return clazz.kotlin.isData
    }

}