package mandysax.anna2.bean;

import mandysax.anna2.annotation.Value;

/**
 * @author 黄浩
 * <p>
 * For requests that don't care about the returned result
 *
 */
@SuppressWarnings("unused")
public final class ResponseBody {
    @Value("code")
    public int code;
}
