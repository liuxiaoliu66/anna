package mandysax.anna2.utils

sealed class ReturnType {
    data class ArrayModel(val clazz: Class<*>) : ReturnType()

    data class ListModel(val clazz: Class<*>) : ReturnType()

    data class Model(val clazz: Class<*>) : ReturnType()

    data class Anna(val clazz: Class<*>) : ReturnType()
}
