package mandysax.anna2.utils

import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import org.json.JSONTokener

/**
 * @author 黄浩
 */
internal object Utils {

    fun parsing(json: String, paths: List<String>): String {
        var s = json
        for (name in paths) {
            try {
                s = when (JSONTokener(s).nextValue() is JSONObject) {
                    true -> JSONObject(s).optString(name)
                    false -> JSONArray(s).optJSONObject(0).optString(name)
                }
            } catch (e: JSONException) {
                return s
            }
        }
        return s
    }

}