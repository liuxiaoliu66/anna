package mandysax.anna2.annotation

/**
 * @author 黄浩
 */
@Target(AnnotationTarget.FUNCTION)
@Retention(AnnotationRetention.RUNTIME)
annotation class Header(val value: String)