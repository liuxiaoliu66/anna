package mandysax.anna2.annotation

/**
 * @author 黄浩
 */
@Target(AnnotationTarget.FIELD)
@Retention(AnnotationRetention.RUNTIME)
@MustBeDocumented
annotation class Value(val value: String)