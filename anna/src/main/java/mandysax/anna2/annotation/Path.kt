package mandysax.anna2.annotation

/**
 * @author 黄浩
 */
@Target(
    AnnotationTarget.FIELD,
    AnnotationTarget.FUNCTION,
)
@Retention(AnnotationRetention.RUNTIME)
annotation class Path(val value: String)