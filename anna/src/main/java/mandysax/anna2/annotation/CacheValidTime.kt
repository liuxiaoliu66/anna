package mandysax.anna2.annotation

import java.util.concurrent.TimeUnit

/**
 * @author 黄浩
 */
@Target(AnnotationTarget.FUNCTION)
@Retention(AnnotationRetention.RUNTIME)
annotation class CacheValidTime(val duration: Long, val unit: TimeUnit = TimeUnit.MILLISECONDS)
