package mandysax.anna2.annotation

/**
 * @author 黄浩
 */
@Target(AnnotationTarget.VALUE_PARAMETER)
@Retention(AnnotationRetention.RUNTIME)
annotation class Part(val value: String)