package mandysax.anna2.annotation

import com.drake.net.cache.CacheMode

/**
 * @author 黄浩
 */
@Target(AnnotationTarget.FUNCTION)
@Retention(AnnotationRetention.RUNTIME)
annotation class EnableCache(val mode: CacheMode)
