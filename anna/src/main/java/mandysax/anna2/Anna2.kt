package mandysax.anna2

import com.drake.net.cache.CacheMode
import mandysax.anna2.annotation.CacheValidTime
import mandysax.anna2.annotation.EnableCache
import mandysax.anna2.annotation.Path
import mandysax.anna2.factory.ConverterFactory
import mandysax.anna2.utils.ReturnType
import java.lang.reflect.GenericArrayType
import java.lang.reflect.Method
import java.lang.reflect.ParameterizedType
import java.lang.reflect.Proxy
import java.lang.reflect.Type
import java.lang.reflect.WildcardType
import java.util.concurrent.TimeUnit
import kotlin.coroutines.Continuation

/**
 * @author 黄浩
 */
@Suppress("unused")
class Anna2 internal constructor() {

    var mBaseUrl: String? = null
        private set

    //缓存模式
    var mCacheMode: CacheMode? = null
        private set

    //缓存时间
    var mCacheDuration: Long? = null
        private set

    //缓存时间单位
    var mCacheUnit: TimeUnit? = null
        private set

    /**
     * 设置[Anna2]的基础URL
     */
    fun baseUrl(baseUrl: String): Anna2 {
        this.mBaseUrl = baseUrl
        return this
    }

    /**
     * 设置[Anna2]的缓存模式
     */
    private fun cacheMode(mode: CacheMode): Anna2 {
        mCacheMode = mode
        return this
    }

    /**
     * 设置[Anna2]的有效时长
     */
    private fun cacheValidTime(duration: Long, unit: TimeUnit = TimeUnit.MILLISECONDS): Anna2 {
        mCacheDuration = duration
        mCacheUnit = unit
        return this
    }

    fun addConverterFactory(factory: ConverterFactory.Factory): Anna2 {
        ConverterFactory.add(factory)
        return this
    }

    @Suppress("UNCHECKED_CAST")
    fun <T> create(clazz: Class<T>): T {
        if (mBaseUrl != null && mBaseUrl?.isEmpty() == true) {
            throw IllegalArgumentException("base url required")
        }
        return Proxy.newProxyInstance(
            clazz.classLoader,
            arrayOf<Class<*>>(clazz)
        ) { _, method, args ->
            if (args == null || args[args.size - 1] !is Continuation<*>) {
                throw IllegalArgumentException("Please add suspend annotation to fun")
            }
            fun handleModel(returnType: ReturnType): Any {
                val functionAnnotations = method.annotations
                val valueParameter = HashMap<Annotation, Any>()
                val annotations = method.parameterAnnotations
                for (i in annotations.indices) {
                    if (args[i] is Continuation<*>)
                        continue
                    valueParameter[annotations[i][0]] = args[i]
                }
                val baseRequest = HttpServiceMethod.create(
                    this,
                    functionAnnotations,
                    valueParameter
                )
                val continuation = args[args.size - 1] as Continuation<Any>
                return ModelBuildServiceJava.build(
                    baseRequest,
                    returnType,
                    method.getAnnotation(Path::class.java)?.value,
                    continuation
                )
            }
            when (val returnType = getReturnType(method, clazz)) {
                is ReturnType.Anna -> {
                    val anna2 = mAnnaCacheMap[method.toString()] ?: Anna2().also {
                        mAnnaCacheMap[method.toString()] = it
                    }
                    anna2.baseUrl(mBaseUrl!!)
                    method.getAnnotation(EnableCache::class.java)?.let {
                        anna2.cacheMode(it.mode)
                    }
                    method.getAnnotation(CacheValidTime::class.java)?.let {
                        anna2.cacheValidTime(it.duration, it.unit)
                    }
                    return@newProxyInstance anna2.create(clazz)
                }

                else -> {
                    handleModel(returnType)
                }
            }
        } as T
    }

    private fun getReturnType(method: Method, clazz: Class<*>): ReturnType {
        //使用suspend修饰后，函数的返回类型被修改为Any，具体的返回类型被移动到函数上的最后一个参数的泛型类型
        //这么说还不明白的话可以参考：https://mp.weixin.qq.com/s?__biz=MzIzNTc5NDY4Nw==&mid=2247484584&idx=1&sn=14deb55b443975eec648602a8807862e&chksm=e8e0fd38df97742e45daa2cba014bbbde2216212452c72a00e94e100c13d90d2af5d3fa9ed61&token=981517486&lang=zh_CN#rd
        val type: Type = method.genericParameterTypes[method.genericParameterTypes.size - 1]
        if (type is ParameterizedType) {
            val step1 = type.actualTypeArguments[0] as WildcardType
            //? super java.util.ArrayList<studio.mandysa.anna.BannerModel>
            when (val unknownType = step1.lowerBounds[0]) {
                is ParameterizedType -> {
                    //java.util.ArrayList<studio.mandysa.anna.BannerModel>
                    if (unknownType.rawType == List::class.java || unknownType.rawType == ArrayList::class.java) {
                        /*class studio.mandysa.anna.BannerModel*/
                        return ReturnType.ListModel(unknownType.actualTypeArguments[0] as Class<*>)
                    }
                }

                clazz -> {
                    return ReturnType.Anna(clazz)
                }

                is GenericArrayType -> {
                    return ReturnType.ArrayModel(unknownType.genericComponentType as Class<*>)
                }

                else -> {
                    //class studio.mandysa.anna.BannerModel
                    return ReturnType.Model(unknownType as Class<*>)
                }
            }
        }
        throw RuntimeException("unsupported return type")
    }

    companion object {

        private val mAnnaCacheMap = HashMap<String, Anna2>()

        @JvmStatic
        fun build(): Anna2 {
            return Anna2()
        }
    }
}