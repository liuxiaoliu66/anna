package mandysax.anna2

import com.drake.net.Net
import com.drake.net.cache.CacheMode
import com.drake.net.request.BaseRequest
import com.drake.net.request.BodyRequest
import com.drake.net.request.UrlRequest
import mandysax.anna2.annotation.Body
import mandysax.anna2.annotation.CacheValidTime
import mandysax.anna2.annotation.Delete
import mandysax.anna2.annotation.EnableCache
import mandysax.anna2.annotation.Field
import mandysax.anna2.annotation.FormUrlEncoded
import mandysax.anna2.annotation.Get
import mandysax.anna2.annotation.Head
import mandysax.anna2.annotation.Header
import mandysax.anna2.annotation.Headers
import mandysax.anna2.annotation.Options
import mandysax.anna2.annotation.Part
import mandysax.anna2.annotation.Patch
import mandysax.anna2.annotation.Post
import mandysax.anna2.annotation.Put
import mandysax.anna2.annotation.Query
import mandysax.anna2.annotation.Trace
import okhttp3.MultipartBody
import okhttp3.RequestBody
import java.util.concurrent.TimeUnit
import com.drake.net.request.Method as NetMethod


/**
 * @author 黄浩
 */
internal object HttpServiceMethod {

    fun create(
        anna2: Anna2,
        functionAnnotations: Array<Annotation>,
        valueParameter: HashMap<Annotation, Any>
    ): BaseRequest {
        //请求头
        val headerBuilder = okhttp3.Headers.Builder()
        //请求类型
        var netMethod: NetMethod = NetMethod.GET
        //具体请求的url
        var url: String? = null
        //启用编码
        var encoded = false
        //缓存模式
        var cacheMode: CacheMode? = anna2.mCacheMode
        //缓存时间
        var cacheDuration: Long? = anna2.mCacheDuration
        //缓存时间单位
        var cacheUnit: TimeUnit? = anna2.mCacheUnit

        for (annotation in functionAnnotations) {
            when (annotation) {
                is Headers -> {
                    val values = annotation.value.split(":")
                    headerBuilder.add(values[0].trim(), values[1].trim())
                }

                is Get -> {
                    url = anna2.mBaseUrl + annotation.value
                    netMethod = NetMethod.GET
                }

                is Post -> {
                    url = anna2.mBaseUrl + annotation.value
                    netMethod = NetMethod.POST
                }

                is Delete -> {
                    url = anna2.mBaseUrl + annotation.value
                    netMethod = NetMethod.DELETE
                }

                is Put -> {
                    url = anna2.mBaseUrl + annotation.value
                    netMethod = NetMethod.PUT
                }

                is Head -> {
                    url = anna2.mBaseUrl + annotation.value
                    netMethod = NetMethod.HEAD
                }

                is Options -> {
                    url = anna2.mBaseUrl + annotation.value
                    netMethod = NetMethod.OPTIONS
                }

                is Trace -> {
                    url = anna2.mBaseUrl + annotation.value
                    netMethod = NetMethod.TRACE
                }

                is Patch -> {
                    url = anna2.mBaseUrl + annotation.value
                    netMethod = NetMethod.PATCH
                }

                is FormUrlEncoded -> {
                    encoded = true
                }

                is EnableCache -> {
                    cacheMode = annotation.mode
                }

                is CacheValidTime -> {
                    cacheDuration = annotation.duration
                    cacheUnit = annotation.unit
                }
            }
        }
        if (url == null)
            throw NullPointerException("base is null")
        fun BaseRequest.setHeaders() {
            if (cacheMode != null)
                setCacheMode(cacheMode)
            if (cacheDuration != null && cacheUnit != null)
                setCacheValidTime(cacheDuration, cacheUnit)
            setHeaders(headerBuilder.build())
        }

        val function1: BodyRequest.() -> Unit = {
            for (entry in valueParameter) {
                when (val annotation = entry.key) {
                    is Query -> {
                        addQuery(annotation.value, entry.value.safeToString(), encoded)
                    }

                    is Body -> {
                        param(entry.value as RequestBody)
                    }

                    is Header -> {
                        headerBuilder.add(annotation.value, entry.value.toString())
                    }

                    is Field -> {
                        param(annotation.value, entry.value.safeToString(), encoded)
                    }

                    is Part -> {
                        param(entry.value as MultipartBody.Part)
                    }
                }
            }
            setHeaders()
        }
        val function: UrlRequest.() -> Unit = {
            for (entry in valueParameter) {
                when (val annotation = entry.key) {
                    is Query -> {
                        addQuery(annotation.value, entry.value.safeToString(), encoded)
                    }

                    is Header -> {
                        headerBuilder.add(annotation.value, entry.value.toString())
                    }
                }
            }
            setHeaders()
        }
        return when (netMethod) {
            NetMethod.GET -> {
                Net.get(url, block = function)
            }

            NetMethod.HEAD -> {
                Net.head(url, block = function)
            }

            NetMethod.OPTIONS -> {
                Net.options(url, block = function)
            }

            NetMethod.TRACE -> {
                Net.trace(url, function)
            }

            NetMethod.POST -> {
                Net.post(url, block = function1)
            }

            NetMethod.DELETE -> {
                Net.delete(url, block = function1)
            }

            NetMethod.PUT -> {
                Net.put(url, block = function1)
            }

            NetMethod.PATCH -> {
                Net.patch(url, block = function1)
            }
        }
    }

    /**
     * 对object为null或者类型为[List]的变量进行处理
     */
    private fun Any?.safeToString(): String {
        if (this == null) return "null"
        when (this) {
            is List<*> -> {
                val s = this.toString()
                if (s.isNotEmpty() && s[0] == '[' && s.endsWith("]")) {
                    return s.substring(1, s.length - 1)
                }
                return s
            }

            else -> return this.toString()
        }
    }

}