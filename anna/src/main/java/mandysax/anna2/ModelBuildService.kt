package mandysax.anna2

import com.drake.net.request.BaseRequest
import kotlinx.coroutines.suspendCancellableCoroutine
import mandysax.anna2.factory.ConverterFactory
import mandysax.anna2.utils.ReturnType
import mandysax.anna2.utils.Utils.parsing
import okhttp3.Call
import okhttp3.Callback
import okhttp3.Response
import org.json.JSONArray
import org.json.JSONException
import java.io.IOException
import java.lang.reflect.Array
import kotlin.coroutines.resume
import kotlin.coroutines.resumeWithException

/**
 * @author 黄浩
 */
internal object ModelBuildService {
    @JvmStatic
    suspend fun build(
        baseRequest: BaseRequest,
        returnType: ReturnType,
        path: String?
    ): Any {
        val returnFunction: (String) -> Any = { json ->
            when (returnType) {

                is ReturnType.ArrayModel -> {
                    val jsonArray: JSONArray = try {
                        JSONArray(json)
                    } catch (e: JSONException) {
                        JSONArray()
                    }
                    val array = Array.newInstance(returnType.clazz, jsonArray.length())
                    for (i in 0 until jsonArray.length()) {
                        val bean = ConverterFactory.create(returnType.clazz, jsonArray.getString(i))
                        Array.set(array, i, bean)
                    }
                    array
                }

                is ReturnType.ListModel -> {
                    val jsonArray: JSONArray = try {
                        JSONArray(json)
                    } catch (e: JSONException) {
                        JSONArray()
                    }
                    val beans = arrayListOf<Any>()
                    for (i in 0 until jsonArray.length()) {
                        val bean = ConverterFactory.create(returnType.clazz, jsonArray.getString(i))
                        beans.add(bean)
                    }
                    beans
                }

                is ReturnType.Model -> {
                    ConverterFactory.create(returnType.clazz, json)
                }

                else -> {
                    throw RuntimeException()
                }
            }
        }
        return suspendCancellableCoroutine { continuation ->
            val call = baseRequest.enqueue(object : Callback {
                override fun onFailure(call: Call, e: IOException) {
                    continuation.resumeWithException(e)
                }

                override fun onResponse(call: Call, response: Response) {
                    val json = response.body!!.string()
                    val instance =
                        returnFunction.invoke(path?.split("/")?.let { parsing(json, it) } ?: json)
                    continuation.resume(instance)
                }

            })
            continuation.invokeOnCancellation {
                if (!call.isCanceled()) {
                    call.cancel()
                }
            }
        }
    }
}