package mandysax.anna2;

import com.drake.net.request.BaseRequest;

import kotlin.coroutines.Continuation;
import mandysax.anna2.utils.ReturnType;

/**
 * @author 黄浩
 */
public final class ModelBuildServiceJava {
    private ModelBuildServiceJava() {
        //not instantiable
    }

    public static Object build(BaseRequest baseRequest,
                               ReturnType returnType,
                               String path,
                               Continuation<? super Object> continuation) {
        return ModelBuildService.build(baseRequest, returnType, path, continuation);
    }
}
